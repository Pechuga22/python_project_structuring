import importlib

from src.config import WORKFLOWS


class ProcessExecutor:
    @classmethod
    def run_process(cls, process_name: str, *args, **kwargs):
        """
        Takes a process_name and run the corresponding workflow
        :param process_name: The name of the workflow registered at WORKFLOWS Map
        :return: None
        """

        processor_module = importlib.import_module(WORKFLOWS[process_name])
        return getattr(processor_module, process_name).run_process(*args, **kwargs)
