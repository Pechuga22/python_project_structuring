from abc import ABC, abstractmethod


class Process(ABC):
    """
    Abstract class to wrap the unique entry point of any Process sub-class
    """

    @classmethod
    @abstractmethod
    def run_process(cls, *args, **kwargs):
        pass
