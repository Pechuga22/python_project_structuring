class TextUtil:
    @staticmethod
    def trim_text(text: str):
        """
        Remove spaces and tabs from the beginning and end of a text

        ' Cloud Computing Colombia' -> 'Cloud Computing Colombia'
        'Python Structuring     ' -> 'Python Structuring'
        :param text: the text to transform
        :return: the text cleaned
        """
        return text.strip().replace('\t', '')
