from functools import reduce


class IterUtil:
    @staticmethod
    def chunk_iterable(seq: iter, chunk_size: int):
        """
        Turns a list into an iterator yielding parts depending on the chunk_size given
        :param seq: list of data
        :param chunk_size: the size of the each chunk
        :return: yields every part of the iterator generated

        Example:
        : seq: An object of type iter:
            eg: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        :param chunk_size: The length on each chunk to build the iterator.
            eg: 5
        :return [1, 2, 3, 4, 5] ... [6, 7, 8, 9, 10] ... [11, 12]
        """
        for pos in range(0, len(seq), chunk_size):
            yield seq[pos:pos + chunk_size]

    @staticmethod
    def join_lists(list_of_lists, remove_dups=False):
        """
        Takes a lists of lists and reduces it into a single list
        :param remove_dups: indicates if should remove duplicated items after reduction
        :param list_of_lists: the iterator containing all the lists to be joined
        :return: a flat list with the elements of each single list

        IN -> [[1,2], [1], [4,5]]
        OUT -> [1,2,1,4,5] if remove_dups = False
        OUT -> [1,2,4,5] if remove_dups = True
        """
        if remove_dups:
            return list(set(reduce(lambda x, y: [*x, *y], list_of_lists)))
        return list(reduce(lambda x, y: [*x, *y], list_of_lists))