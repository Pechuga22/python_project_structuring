class OwnException(Exception):
    def __init__(self, code: str, name: str, description: str, path: str, extra_info: dict = None):
        self._code = code
        self._name = name
        self._description = description
        self._path = path
        self._extra_info = extra_info

    @property
    def code(self):
        return self._code

    @code.setter
    def code(self, code):
        self._code = code

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        self._name = name

    @property
    def description(self):
        return self._description

    @description.setter
    def description(self, description):
        self._description = description

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, path):
        self._path = path

    @property
    def extra_info(self):
        return self._extra_info

    @extra_info.setter
    def extra_info(self, extra_info):
        self._extra_info = extra_info

    def __str__(self):
        return f"\n  Code: {self.code}\n  Name: {self.name}\n  Raised at: {self.path}\n  " \
               f"Description: {self.description}\n  Extra Info: {self.extra_info or ''}"
