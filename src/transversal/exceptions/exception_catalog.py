from src.transversal.exceptions.exception import OwnException


class ProcessUndefinedException(OwnException):
    def __init__(self, description: str, path: str, extra_info: dict = None):
        super().__init__(
            "PN-001",
            "Integrity Exception",
            description, path, extra_info
        )


class SourceNotFoundException(OwnException):
    def __init__(self, description: str, path: str, extra_info: dict = None):
        super().__init__(
            "HNE-002",
            "Source Not Found",
            description, path, extra_info
        )
