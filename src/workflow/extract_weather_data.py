import json

import requests

from src.config import OPENWEATHER_API_KEY
from src.process.process import Process


class ExtractWeatherData(Process):
    _base_endpoint = 'https://api.openweathermap.org/data/2.5/onecall'

    @classmethod
    def run_process(cls, lat: str, lon: str, exclude: str = None, *args, **kwargs):
        """
        Extracts info from the Open Weather API and stores it in a proper form
        API endpoint: https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&exclude={part}&appid={API key}
        :param: lat. The Latitude on the map. e.g '6.2476'
        :param: lon. The Longitude on the map. e.g '75.5658'
        :param: exclude. a comma separated list of parameter to exclude in the query. e.g 'hourly,daily'. Valid
                parameters: 'current', 'minutely', 'hourly', 'daily', 'alerts'
        :return: None
        """
        url = f'{cls._base_endpoint}?lat={lat}&lon={lon}&appid={OPENWEATHER_API_KEY}'
        if exclude:
            url = f"{url}&exclude={exclude}"

        print(url)

        result = requests.get(url)

        print(json.dumps(json.loads(result.text)))
