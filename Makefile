clean:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +
	clear

clean-venv:
	pipenv uninstall --all

install-local:
	clean-venv
	pipenv install --dev

install:
	clean-venv
	pipenv install

build-docker:
	docker build -t josemhenao/python_structuring:alpha .
	docker push josemhenao/python_structuring:alpha

