from src.transversal.utils.iter_util import IterUtil


class TestIterUtil:
    def test_join_lists(self):
        # Arrange
        dummy_list = [[1, 2], ['A', 'B'], ['C']]
        supposed_transformed_list = [1, 2, 'A', 'B', 'C']
        # Act
        transformed_list = IterUtil.join_lists(list_of_lists=dummy_list)
        # Assert
        assert transformed_list == supposed_transformed_list

    def test_join_lists_removing_dups(self):
        dummy_list = [[1, 2], ['A', 'B'], ['C', 'A', 1]]
        supposed_transformed_list = list({1, 2, 'A', 'B', 'C'})

        transformed_list = IterUtil.join_lists(list_of_lists=dummy_list, remove_dups=True)

        assert transformed_list == supposed_transformed_list
