from src.transversal.utils.text_util import TextUtil


class TestTextUtil:
    def tests_trim_text_without_spaces(self):
        # Arrange
        dummy_text = "Cloud Computing Colombia"
        expected_result = "Cloud Computing Colombia"
        # Act
        cleaned_text = TextUtil.trim_text(dummy_text)
        # Assert
        assert cleaned_text == expected_result

    def tests_trim_text_with_spaces(self):
        # Arrange
        dummy_text = " Cloud Computing Colombia  "
        expected_result = "Cloud Computing Colombia"
        # Act
        cleaned_text = TextUtil.trim_text(dummy_text)
        # Assert
        assert cleaned_text == expected_result

    def tests_trim_text_with_tabs(self):
        # Arrange
        dummy_text = "Cloud Computing Colombia     "
        expected_result = "Cloud Computing Colombia"
        # Act
        cleaned_text = TextUtil.trim_text(dummy_text)
        # Assert
        assert cleaned_text == expected_result

    def tests_trim_text_with_tabs_and_spaces(self):
        # Arrange
        dummy_text = "   Cloud Computing Colombia     "
        expected_result = "Cloud Computing Colombia"
        # Act
        cleaned_text = TextUtil.trim_text(dummy_text)
        # Assert
        assert cleaned_text == expected_result
